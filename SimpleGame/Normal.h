#pragma once
#include "Struct.h"

class Normal
{
public:
	Normal();
	~Normal();

public:
	void InitPysics();

	void SetPos(float xPos, float yPos, float zPos);
	void SetVol(float xVol, float yVol, float zVol);
	void SetType(int type);
	void SetColor(float r, float g, float b, float a);

	void SetTextureID(int id);

	void SetIsCollision(bool b) { m_isCollision = b; }

	Position GetPos() const { return m_pos; }
	Volume GetVol() const { return m_vol; }
	Color GetCol() const { return m_col; }
	int GetType() const { return m_type; }
	int GetTextureID() const { return m_textureID; }
	bool GetIsCollision() const { return m_isCollision; }

private:
	Position m_pos;
	Volume m_vol;
	Color m_col;
	int m_type;
	int m_textureID = -1;

	bool m_isCollision = false;
};

