#include "stdafx.h"
#include "Normal.h"

Normal::Normal()
{
}


Normal::~Normal()
{
}

void Normal::InitPysics()
{
	m_pos = { 0.f, 0.f, 0.f };
	m_vol = { 0.f, 0.f, 0.f };
}

void Normal::SetPos(float xPos, float yPos, float zPos)
{
	m_pos.xPos = xPos;
	m_pos.yPos = yPos;
	m_pos.zPos = zPos;
}

void Normal::SetVol(float xVol, float yVol, float zVol)
{
	m_vol.xVol = xVol;
	m_vol.yVol = yVol;
	m_vol.zVol = zVol;
}

void Normal::SetType(int type)
{
	m_type = type;
}

void Normal::SetColor(float r, float g, float b, float a)
{
	m_col.red = r;
	m_col.green = g;
	m_col.blue = b;
	m_col.alpha = a;
}

void Normal::SetTextureID(int id)
{
	m_textureID = id;
}
