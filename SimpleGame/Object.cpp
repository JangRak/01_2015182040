#include "stdafx.h"
#include "Object.h"
#include <math.h>
#include <iostream>

Object::Object()
{
	InitPysics();
	m_remainingBulletCoolTime = m_defaultBulletCoolTime;
	m_age = rand() % 10;
	//m_age = 0.f;
}

Object::~Object()
{

}

// Init
void Object::InitPysics()
{
	m_pos = { 0.f, 0.f, 0.f };
	m_vel = { 0.f, 0.f, 0.f };
	m_vol = { 0.f, 0.f, 0.f };
	m_acc = { 0.f, 0.f, 0.f };
	m_color = { 0.f, 0.f, 0.f };
	m_mass = -1.f;
	m_frictCoef = 0.f;
}

// Set
void Object::SetPos(float xPos, float yPos, float zPos)
{
	m_pos.xPos = xPos;
	m_pos.yPos = yPos;
	m_pos.zPos = zPos;
}

void Object::SetVel(float xVel, float yVel, float zVel)
{
	m_vel.xVel = xVel;
	m_vel.yVel = yVel;
	m_vel.zVel = zVel;
}

void Object::SetVol(float xVol, float yVol, float zVol)
{
	m_vol.xVol = xVol;
	m_vol.yVol = yVol;
	m_vol.zVol = zVol;
}

void Object::SetAcc(float xAcc, float yAcc, float zAcc)
{
	m_acc.xAcc = xAcc;
	m_acc.yAcc = yAcc;
	m_acc.zAcc = zAcc;
}

void Object::SetMass(float mass)
{
	m_mass = mass;
}

void Object::SetColor(float red, float green, float blue, float alpha)
{
	m_color.red = red;
	m_color.green = green;
	m_color.blue = blue;
	m_color.alpha = alpha;
}

void Object::SetFrictCoef(float coef)
{
	m_frictCoef = coef;
}

void Object::SetType(int type)
{
	m_type = type;
}

void Object::SetTextureID_right(int id)
{
	m_textureID_right = id;
}

void Object::SetTextureID_left (int id)
{
	m_textureID_left = id;
}

void Object::SetTextureID_idle(int id)
{
	m_textureID_idle = id;
}

void Object::SetTextureID(int id)
{
	m_textureID = id;
}

void Object::SetParentObj(Object * obj)
{
	m_parent = obj;
}

void Object::SetDirection(int dir)
{
	m_direction = dir;
}

void Object::SetMoveDir(int dir)
{
	m_moveDir = dir;
}

void Object::SetPresentMode(int m)
{
	m_presentMode = m;
}

void Object::SetDieTime(float time)
{
	m_dieTime = time;
}

bool Object::CanShootBullet()
{
	if (m_remainingBulletCoolTime < 0.0000000001f) {
		ResetShootBulletCoolTime();
		return true;
	}
	else
		return false;
}

bool Object::CanShootPresent()
{
	if (m_remainingPresentCoolTime < 0.0000000001f) {
		ResetShootPresentCoolTime();
		return true;
	}
	else
		return false;
}

void Object::ResetShootBulletCoolTime()
{
	m_remainingBulletCoolTime = m_defaultBulletCoolTime;
}

void Object::CheckDamage()
{
	float time = m_age - m_damageTime;

	if (time > 2.f) {
		m_isDamage = false;
	}
}

void Object::CheckAlive()
{
	float time = m_age - m_dieTime;

	if (time > 5.f) {
		m_isDie = false;
		m_currentHealthPoint = m_maxHealthPoint;
	}
}

void Object::ResetShootPresentCoolTime()
{
	m_remainingPresentCoolTime = m_defaultPresentCoolTime;
}

void Object::Damage(float damage)
{
	m_currentHealthPoint -= damage;
}

void Object::SetHP(float hp)
{
	m_maxHealthPoint = hp;
	m_currentHealthPoint = m_maxHealthPoint;
}

float Object::GetHP() const
{
	return m_currentHealthPoint;
}

float Object::GetHPGauge() const
{
	return m_currentHealthPoint / m_maxHealthPoint;
}

float Object::GetAge() const
{
	return m_age;
}

bool Object::IsAncestor(Object* obj)
{
	// TBD
	if (m_parent != NULL) {
		if (obj == m_parent) {
			return true;
		}
	}
	return false;
}

int Object::GetTextureID() const
{
	if (m_type == TYPE_BULLET || m_type == TYPE_PRESENT) {
		return m_textureID;
	}

	switch (m_direction) {
	case RIGHT:
		return m_textureID_right;
	case LEFT:
		return m_textureID_left;
	case IDLE:
		return m_textureID_idle;
	default:
		std::cout << "Direction Type error" << std::endl;
		while (true);
	}
}

void Object::Update(float elapsedTime)
{
	// Reduce bullet cooltime
	m_remainingBulletCoolTime -= elapsedTime;
	m_remainingPresentCoolTime -= elapsedTime;

	m_age += elapsedTime;

	if (true == m_isDamage) CheckDamage();
	if (true == m_isDie) CheckAlive();

	//////// Apply Friction ////////
	// normalize velocity vector(only x, y)
	float velSize = sqrtf(m_vel.xVel * m_vel.xVel + m_vel.yVel * m_vel.yVel);
	if (velSize > 0.f) {
		float xV = m_vel.xVel / velSize;
		float yV = m_vel.yVel / velSize;

		// calculate friction size
		float normalForce = GRAVITY * m_mass;	// 수직항력 -> 2D 게임이라 여기까지
		float frictionSize = m_frictCoef * normalForce;
		float xForce = -xV * frictionSize;
		float yForce = -yV * frictionSize;

		// calculate acc from friction
		float xAcc = xForce / m_mass;
		float yAcc = yForce / m_mass;

		// calculate velocity
		float new_xVel = m_vel.xVel + (xAcc * elapsedTime);
		float new_yVel = m_vel.yVel + (yAcc * elapsedTime);
		m_vel.zVel = m_vel.zVel - GRAVITY * elapsedTime;
		if (new_xVel * m_vel.xVel < 0.f) {
			m_vel.xVel = 0.f;
		}
		else {
			m_vel.xVel = new_xVel;
		}
		if (new_yVel * m_vel.yVel < 0.f) {
			m_vel.yVel = 0.f;
		}
		else {
			m_vel.yVel = new_yVel;
		}
	}
	else if (m_pos.zPos > 0.f) {
		m_vel.zVel = m_vel.zVel - GRAVITY * elapsedTime;
	}
	////////////////////////////////
	m_pos.xPos = m_pos.xPos + (m_vel.xVel * elapsedTime);
	m_pos.yPos = m_pos.yPos + (m_vel.yVel * elapsedTime);
	m_pos.zPos = m_pos.zPos + (m_vel.zVel * elapsedTime);

	if (m_pos.zPos < 0.f) {
		m_pos.zPos = 0.f;
		m_vel.zVel = 0.f;
	}

	if (m_vel.xVel == 0.f && m_vel.yVel == 0.f && m_vel.zVel == 0.f) {
		m_direction = IDLE;
	}

	if (m_type == TYPE_PRESENT || m_type == TYPE_BULLET) return;

	if (m_pos.xPos > 30.0f) {
		m_pos.xPos = 30.0f;
	}
	if (m_pos.xPos < -30.0f) {
		m_pos.xPos = -30.0f;
	}
	if (m_pos.yPos > 30.0f) {
		m_pos.yPos = 30.0f;
	}
	if (m_pos.yPos < -30.0f) {
		m_pos.yPos = -30.0f;
	}
}

void Object::FindHERO(Object* hero)
{
	if (true == m_isDie) {
		m_state = PEACE;
		return;
	}
	POSITION h_p = hero->GetPos();

	float distance =
		((m_pos.xPos - h_p.xPos) * (m_pos.xPos - h_p.xPos) +
		 (m_pos.yPos - h_p.yPos) * (m_pos.yPos - h_p.yPos));

	if (distance > 30.f) m_state = PEACE;
	else m_state = CHASING;
}

void Object::AddForce(float x, float y, float z, float elapsedTime)
{
	float xAcc, yAcc, zAcc;
	xAcc = yAcc = zAcc = 0.f;

	xAcc = x / m_mass;
	yAcc = y / m_mass;
	zAcc = z / m_mass;

	m_vel.xVel = m_vel.xVel + xAcc * elapsedTime;
	m_vel.yVel = m_vel.yVel + yAcc * elapsedTime;
	m_vel.zVel = m_vel.zVel + zAcc * elapsedTime;

	if (m_type == TYPE_HERO) {
		if (m_vel.xVel > 4.5f) {
			m_vel.xVel = 4.5f;
		}
		if (m_vel.xVel < -4.5f) {
			m_vel.xVel = -4.5f;
		}
		if (m_vel.yVel > 4.5f) {
			m_vel.yVel = 4.5f;
		}
		if (m_vel.yVel < -4.5f) {
			m_vel.yVel = -4.5f;
		}
		if (m_vel.zVel > 4.5f) {
			m_vel.zVel = 4.5f;
		}
		if (m_vel.zVel < -4.5f) {
			m_vel.zVel = -4.5f;
		}
	}
	else {
		if (m_vel.xVel > 2.5f) {
			m_vel.xVel = 2.5f;
		}
		if (m_vel.xVel < -2.5f) {
			m_vel.xVel = -2.5f;
		}
		if (m_vel.yVel > 2.5f) {
			m_vel.yVel = 2.5f;
		}
		if (m_vel.yVel < -2.5f) {
			m_vel.yVel = -2.5f;
		}
		if (m_vel.zVel > 2.5f) {
			m_vel.zVel = 2.5f;
		}
		if (m_vel.zVel < -2.5f) {
			m_vel.zVel = -2.5f;
		}
	}
}