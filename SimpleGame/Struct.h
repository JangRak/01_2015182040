#pragma once
//
enum STATE { READY, PLAY, FAIL, END, INIT };

enum ENEMY_STATE { CHASING, PEACE };

const int MAX_ALIVE_OBJECT{ 50 };
const int ENEMY_END_NUM{ 11 };
const int MAX_NORMAL_OBJECT{ 111 };
const int OBSTACLE_START_NUM{ 14 };

typedef struct POSITION {
	float xPos;
	float yPos;
	float zPos;
}Position;

typedef struct COLOR {
	float red;
	float green;
	float blue;
	float alpha;
}Color;

typedef struct VELOCITY {
	float xVel;
	float yVel;
	float zVel;
}Velocity;

typedef struct ACCELERATION {
	float xAcc;
	float yAcc;
	float zAcc;
}Acceleration;

typedef struct VOLUME {
	float xVol;
	float yVol;
	float zVol;
}Volume;

#define HERO_ID			0

#define GRAVITY			9.8f

#define TYPE_HERO		0
#define TYPE_BULLET		1
#define TYPE_PRESENT	2
#define TYPE_ENEMY		3
#define TYPE_HOUSE		4
#define TYPE_NORMAL		5

#define WORLD_WIDTH		1000.f
#define WORLD_HEIGHT	800.f