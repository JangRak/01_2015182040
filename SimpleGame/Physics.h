#pragma once
#include <iostream>
#include "Object.h"
#include "Normal.h"

class Physics
{
public:
	Physics();
	~Physics();

public:
	bool IsOverlap(Object* A, Object* B, int type = 0);
	bool IsOverlap(Object* A, Normal* B, int type = 0);
	void ProcessCollision(Object* A, Object* B);

private:
	bool BBOverlapTest(Object* A, Object* B);
	bool BBOverlapTest(Object* A, Normal* B);
};