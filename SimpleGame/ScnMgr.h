#pragma once

#include "Renderer.h"
#include "Object.h"
#include "Normal.h"
#include "Struct.h"
#include "Physics.h"
#include "Sound.h"

class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

public:
	int StartState();
	int EndState();
	int FailState();

	int AddObject(float xPos, float yPos, float zPos, float xSize, float ySize, float zSize, float red, float green, float blue, float alpha, float xVel, float yVel, float zVel, float mass, float fricCoef, float hp, int type);
	void DeleteObject(int index);
	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);
	void SpecialKeyDownInput(unsigned char key, int x, int y);
	void SpecialKeyUpInput(unsigned char key, int x, int y);
	void Update(float elapsedTime);
	int RenderScene();
	void DrawGauge(int index, POSITION p, VOLUME v);

private:
	void DoGarbageCollection();

	POSITION m_obstacles[MAX_NORMAL_OBJECT];

	Renderer* m_Renderer = NULL;
	Object* m_Object[MAX_ALIVE_OBJECT];
	Normal* m_Normal[MAX_NORMAL_OBJECT];
	Physics* m_Physics = NULL;
	Sound* m_Sound = NULL;

	int m_presentCount = 0;
	int m_maxPresentCount = 10;

	int m_state = READY;

	// Key inputs
	bool m_keyW = false;
	bool m_keyA = false;
	bool m_keyS = false;
	bool m_keyD = false;
	bool m_keySP = false;

	// Special Key inputs
	bool m_keyUp = false;
	bool m_keyLeft = false;
	bool m_keyDown = false;
	bool m_keyRight = false;

	// textures
	int m_startTexture = -1;
	int m_endTexture = -1;
	int m_failTexture = -1;
	int m_backgroundTexture = -1;
	int m_sideTexture = -1;
	int m_santaRightTexture = -1;
	int m_santaLeftTexture = -1;
	int m_santaIdleTexture = -1;
	int m_enemyRightTexture = -1;
	int m_enemyLeftTexture = -1;
	int m_enemyIdleTexture = -1;
	int m_houseTexture[2] = { -1, -1 };
	int m_snow = -1;
	int m_snowTexture = -1;
	int m_star = -1;
	int m_starTexture = -1;
	int m_presentTexture = -1;
	int m_snowballTexture = -1;
	int m_obstacleTexture[3] = { -1, -1, -1};

	// UI
	int m_numForUITexture = -1;

	// sounds
	int m_backgroundSound = -1;
	int m_shootPresentSound = -1;
	int m_shootSnowballSound = -1;
	int m_goalSound = -1;
	int m_hitSound = -1;
};