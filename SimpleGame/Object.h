#pragma once
#include <math.h>

#include "Struct.h"

enum DIRECTION { RIGHT, LEFT, IDLE };

class Object
{
public:
	Object();
	~Object();

public :
	// Init
	void InitPysics();

	//Set
	void SetPos(float xPos, float yPos, float zPos);
	void SetVel(float xVel, float yVel, float zVel);
	void SetVol(float xVol, float yVol, float zVol);
	void SetAcc(float xAcc, float yAcc, float zAcc);
	void SetMass(float mass);
	void SetColor(float red, float green, float blue, float alpha);
	void SetFrictCoef(float coef);
	void SetType(int type);
	void SetTextureID_right(int id);
	void SetTextureID_left(int id);
	void SetTextureID_idle(int id);
	void SetTextureID(int id);
	void SetParentObj(Object* obj);
	void SetDirection(int dir);
	void SetMoveDir(int dir);
	void SetPresentMode(int m);
	void SetDieTime(float time);
	void SetIsDie(bool b) { m_isDie = b; }
	void SetState(int s) { m_state = s; }
	void SetDamageTime(float time) { m_damageTime = time; }
	void SetIsDamage(bool b) { m_isDamage = b; }

	// Get
	Position GetPos() const { return m_pos; }
	Velocity GetVel() const { return m_vel; }
	Volume GetVol() const { return m_vol; }
	Acceleration GetAcc() const { return m_acc; }
	float GetMass() const { return m_mass; }
	Color GetColor() const { return m_color; }
	float GetFrictCoef() const { return m_frictCoef; }
	int GetType() const { return m_type; }
	int GetTextureID() const;
	int GetMoveDir() const { return m_moveDir; }
	Object* GetParentObj() const { return m_parent; }
	int GetPresentMode() const { return m_presentMode; }
	bool GetIsDie() const { return m_isDie; }
	int GetState() const { return m_state; }
	bool GetIsDamage() const { return m_isDamage; }

	// update
	void Update(float elapsedTime);
	void FindHERO(Object* hero);

	void AddForce(float x, float y, float z, float elapsedTime);

	bool CanShootBullet();
	void ResetShootBulletCoolTime();

	void CheckDamage();
	void CheckAlive();

	bool CanShootPresent();
	void ResetShootPresentCoolTime();

	void Damage(float damage);
	void SetHP(float hp);
	float GetHP() const;
	float GetHPGauge() const;
	float GetAge() const;
	float GetDieTime() const { return m_dieTime; }
	
	
	int GetDirection() const { return m_direction; }

	bool IsAncestor(Object* obj);

private:
	Position m_pos;
	Velocity m_vel;
	Volume m_vol;
	Acceleration m_acc;
	Color m_color;
	float m_mass;
	float m_frictCoef;
	float m_maxHealthPoint;
	float m_currentHealthPoint;
	float m_age;
	int m_type;

	float m_damageTime;
	bool m_isDamage = false;

	float m_dieTime;
	bool m_isDie = false;

	int m_presentMode = TYPE_BULLET;

	Object* m_parent = NULL;

	float m_remainingPresentCoolTime = 0.f;
	float m_defaultPresentCoolTime = 2.f;

	float m_remainingBulletCoolTime = 0.f;
	float m_defaultBulletCoolTime = 0.4f;

	int m_textureID = -1;

	int m_textureID_right = -1;
	int m_textureID_left = -1;
	int m_textureID_idle = -1;

	int m_direction = RIGHT;

	int m_moveDir;

	int m_state = PEACE;
};