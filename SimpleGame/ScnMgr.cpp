#include "stdafx.h"
#include "ScnMgr.h"

ScnMgr::ScnMgr()
{
	m_Renderer = new Renderer(WORLD_WIDTH + 1200, WORLD_HEIGHT + 1200);

	if (!m_Renderer->IsInitialized()) {
		std::cout << "Renderer could not be initialized... \n";
	}
	
	// initialize physics
	m_Physics = new Physics();

	// initialize sound
	m_Sound = new Sound();

	// Create obstacles
	ifstream in{ "obstacles.txt" };
	int index = 0;
	float data = 0;
	while (!in.eof()) {
		in >> data;
		m_obstacles[index].xPos = (float)data;
		in >> data;
		m_obstacles[index].yPos = (float)data;
		++index;
	}
	//Init Object
	for (int i = 0; i < MAX_ALIVE_OBJECT; ++i)
		m_Object[i] = NULL;
	for (int i = 0; i < MAX_NORMAL_OBJECT; ++i)
		m_Normal[i] = NULL;

	// Add Hero object
	m_Object[HERO_ID] = new Object;
	m_Object[HERO_ID]->SetColor(1.f, 1.f, 1.f, 1.f);
	m_Object[HERO_ID]->SetPos(0.f, 0.f, 0.f);
	m_Object[HERO_ID]->SetVol(1.8f, 1.8f, 1.f);
	m_Object[HERO_ID]->SetVel(0.f, 0.f, 0.f);
	m_Object[HERO_ID]->SetMass(1.f);
	m_Object[HERO_ID]->SetFrictCoef(0.7f);
	m_Object[HERO_ID]->SetHP(800.0f);

	m_santaRightTexture = m_Renderer->GenPngTexture("./Textures/santa_right.png");
	m_santaLeftTexture = m_Renderer->GenPngTexture("./Textures/santa_left.png");
	m_santaIdleTexture = m_Renderer->GenPngTexture("./Textures/santa_idle.png");
	m_Object[HERO_ID]->SetTextureID_right(m_santaRightTexture);
	m_Object[HERO_ID]->SetTextureID_left(m_santaLeftTexture);
	m_Object[HERO_ID]->SetTextureID_idle(m_santaIdleTexture);

	m_snowballTexture = m_Renderer->GenPngTexture("./Textures/snowball.png");
	m_presentTexture = m_Renderer->GenPngTexture("./Textures/present.png");

	m_startTexture = m_Renderer->GenPngTexture("./Textures/start.png");
	m_endTexture = m_Renderer->GenPngTexture("./Textures/end.png");
	m_failTexture = m_Renderer->GenPngTexture("./Textures/fail.png");

	m_backgroundTexture = m_Renderer->GenPngTexture("./Textures/bg.png");
	m_sideTexture = m_Renderer->GenPngTexture("./Textures/side.png");

	m_snowTexture = m_Renderer->GenPngTexture("./Textures/particle.png");
	m_starTexture = m_Renderer->GenPngTexture("./Textures/star.png");

	m_enemyRightTexture = m_Renderer->GenPngTexture("./Textures/bad_boy_right.png");
	m_enemyLeftTexture = m_Renderer->GenPngTexture("./Textures/bad_boy_left.png");
	m_enemyIdleTexture = m_Renderer->GenPngTexture("./Textures/bad_boy_idle.png");

	m_houseTexture[0] = m_Renderer->GenPngTexture("./Textures/house.png");
	m_houseTexture[1] = m_Renderer->GenPngTexture("./Textures/house2.png");

	m_obstacleTexture[0] = m_Renderer->GenPngTexture("./Textures/tree1.png");
	m_obstacleTexture[1] = m_Renderer->GenPngTexture("./Textures/tree2.png");
	m_obstacleTexture[2] = m_Renderer->GenPngTexture("./Textures/snowman1.png");

	m_numForUITexture = m_Renderer->GenPngTexture("./Textures/num.png");

	for (int i = 1; i < ENEMY_END_NUM; ++i) {
		float x = (float)(rand() % 30) - 15.f;
		float y = (float)(rand() % 30) - 15.f;
		int temp = AddObject(
			x, y, 0.8f,
			1.5f, 1.5f, 1.5f,
			1.f, 1.f, 1.f, 1.f,
			0.f, 0.f, 0.f,
			1.f,
			0.7f,
			50.f,
			TYPE_ENEMY
		);
		m_Object[temp]->SetTextureID_right(m_enemyRightTexture);
		m_Object[temp]->SetTextureID_left(m_enemyLeftTexture);
		m_Object[temp]->SetTextureID_idle(m_enemyIdleTexture);
		m_Object[temp]->SetMoveDir(rand() % 4);
	}

	// 집
	for (int i = 0; i < OBSTACLE_START_NUM; ++i) {
		m_Normal[i] = new Normal;
		m_Normal[i]->SetPos(m_obstacles[i].xPos, m_obstacles[i].yPos, 0.5f);
		m_Normal[i]->SetVol(1.5f, 2.5f, 1.f);
		m_Normal[i]->SetColor(0.5f, 0.5f, 0.5f, 1.f);
		m_Normal[i]->SetType(TYPE_HOUSE);

		int index = rand() % 2;
		m_Normal[i]->SetTextureID(m_houseTexture[index]);
	}

	// 트리, 눈사람
	for (int i = OBSTACLE_START_NUM; i < MAX_NORMAL_OBJECT - 10; ++i) {
		m_Normal[i] = new Normal;
		m_Normal[i]->SetPos(m_obstacles[i].xPos, m_obstacles[i].yPos, 0.5f);
		m_Normal[i]->SetVol(2.f, 3.f, 1.f);
		m_Normal[i]->SetColor(1.f, 1.f, 1.f, 1.f);
		m_Normal[i]->SetType(TYPE_NORMAL);

		int index = rand() % 2;
		m_Normal[i]->SetTextureID(m_obstacleTexture[index]);
	}

	for (int i = MAX_NORMAL_OBJECT - 10; i < MAX_NORMAL_OBJECT; ++i) {
		m_Normal[i] = new Normal;
		m_Normal[i]->SetPos(m_obstacles[i].xPos, m_obstacles[i].yPos, 0.5f);
		m_Normal[i]->SetVol(1.f, 1.5f, 1.f);
		m_Normal[i]->SetColor(1.f, 1.f, 1.f, 1.f);
		m_Normal[i]->SetType(TYPE_NORMAL);

		m_Normal[i]->SetTextureID(m_obstacleTexture[2]);
	}

	m_backgroundSound = m_Sound->CreateBGSound("./Sounds/bgm.mp3");
	m_Sound->PlayBGSound(m_backgroundSound, true, 1.f);
	m_shootSnowballSound = m_Sound->CreateShortSound("./Sounds/throw.mp3");
	m_goalSound = m_Sound->CreateShortSound("./Sounds/goal.mp3");
	m_hitSound = m_Sound->CreateShortSound("./Sounds/hit.mp3");

	// particle
	// CreateParticleObject -> x, y 는 어디로 보낼지임.
	m_snow = m_Renderer->CreateParticleObject(
		4000,
		-5000.f, -5000.f,
		5000.f, 5000.f,
		25.f, 25.f,
		15.f, 15.f,
		5.f, 5.f,
		10.f, 10.f
	);
	m_star = m_Renderer->CreateParticleObject(
		100,
		-0.f, -0.f,
		0.f, 0.f,
		20.f, 20.f,
		25.f, 25.f,
		-5.f, -5.f,
		0.f, 0.f
	);
}

ScnMgr::~ScnMgr()
{
	if (NULL != m_Renderer) {
		delete m_Renderer;
		m_Renderer = NULL;
	}

	if (NULL != m_Sound) {
		delete m_Sound;
		m_Sound = NULL;
	}

	for (auto& obj : m_Object) {
		if (NULL != obj) {
			delete obj;
			obj = NULL;
		}
	}

	for (auto& nor : m_Normal) {
		if (NULL != nor) {
			delete nor;
			nor = NULL;
		}
	}
}

int ScnMgr::StartState()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	m_Renderer->DrawGround(0.f, 0.f, 0.f, WORLD_WIDTH + 1200.f, WORLD_HEIGHT + 1200.f, 1.f, 1.f, 1.f, 1.f, 1.f, m_startTexture, 0.7f);

	return m_state;
}

int ScnMgr::EndState()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);
	m_Renderer->SetCameraPos(0.f, 0.f * 100.f);
	m_Renderer->DrawGround(0.f, 0.f, 0.f, WORLD_WIDTH + 1400.f, WORLD_HEIGHT + 1400.f, 1.f, 1.f, 1.f, 1.f, 1.f, m_endTexture, 0.7f);

	return m_state;
}

int ScnMgr::FailState()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.8f, 0.8f, 1.0f, 1.0f);
	m_Renderer->SetCameraPos(0.f, 0.f * 100.f);
	m_Renderer->DrawTextureRect(
		0.f, -800.f, 0.f,
		1800.f, 1800.f, 1.f,
		1.f, 1.f, 1.f, 1.f,
		m_failTexture, false
	);

	return m_state;
}

int ScnMgr::AddObject
	(float xPos, float yPos, float zPos, 
		float xSize, float ySize, float zSize, 
		float red, float green, float blue, float alpha, 
		float xVel, float yVel, float zVel, 
		float mass, 
		float fricCoef, 
		float hp,
		int type)
{
	// search empty slot
	int index = -1;

	for (int i = 0; i < MAX_ALIVE_OBJECT; ++i) {
		if (NULL == m_Object[i]) {
			index = i;
			break;
		}
	}
	if (-1 == index) {
		std::cout << "No more remaining object\n";
		return -1;
	}

	m_Object[index] = new Object;
	m_Object[index]->SetColor(red, green, blue, alpha);
	m_Object[index]->SetPos(xPos, yPos, zPos);
	m_Object[index]->SetVol(xSize, ySize, zSize);
	m_Object[index]->SetVel(xVel, yVel, zVel);
	m_Object[index]->SetMass(mass);
	m_Object[index]->SetFrictCoef(fricCoef);
	m_Object[index]->SetType(type);
	m_Object[index]->SetHP(hp);

	return index;
}

void ScnMgr::DeleteObject(int index)
{
	if (index < 0) {
		std::cout << "Negative index does not allowed. index:" << index << std::endl;
		return;
	}

	if (index >= MAX_ALIVE_OBJECT) {
		std::cout << "Requested index exceeds MAX_OBJECT. index:" << index << std::endl;
		return;
	}

	if (NULL != m_Object[index]) {
		delete m_Object[index];
		m_Object[index] = NULL;
	}
}

void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W') {
		if (IDLE == m_Object[HERO_ID]->GetDirection()) {
			m_Object[HERO_ID]->SetDirection(RIGHT);
		}
		m_keyW = true;
	}

	if (key == 'a' || key == 'A') {
		m_Object[HERO_ID]->SetDirection(LEFT);
		m_keyA = true;
	}	

	if (key == 's' || key == 'S') {
		if (IDLE == m_Object[HERO_ID]->GetDirection()) {
			m_Object[HERO_ID]->SetDirection(LEFT);
		}
		m_keyS = true;
	}

	if (key == 'd' || key == 'D') {
		m_Object[HERO_ID]->SetDirection(RIGHT);
		m_keyD = true;
	}

	if (key == ' ') {
		m_keySP = true;
	}

	if (key == 13) {
		if (READY == m_state) {
			m_state = PLAY;
		}
		if (END == m_state || FAIL == m_state) {
			m_state = INIT;
		}
	}
}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W') {
		m_keyW = false;
	}

	if (key == 'a' || key == 'A') {
		m_keyA = false;
	}

	if (key == 's' || key == 'S') {
		m_keyS = false;
	}

	if (key == 'd' || key == 'D') {
		m_keyD = false;
	}

	if (key == ' ') {
		m_keySP = false;
	}
}

void ScnMgr::SpecialKeyDownInput(unsigned char key, int x, int y)
{
	if (key == GLUT_KEY_UP) {
		m_keyUp = true;
	}

	if (key == GLUT_KEY_LEFT) {
		m_keyLeft = true;
	}

	if (key == GLUT_KEY_DOWN) {
		m_keyDown = true;
	}

	if (key == GLUT_KEY_RIGHT) {
		m_keyRight = true;
	}
}

void ScnMgr::SpecialKeyUpInput(unsigned char key, int x, int y)
{
	if (key == GLUT_KEY_UP) {
		m_keyUp = false;
	}

	if (key == GLUT_KEY_LEFT) {
		m_keyLeft = false;
	}

	if (key == GLUT_KEY_DOWN) {
		m_keyDown = false;
	}

	if (key == GLUT_KEY_RIGHT) {
		m_keyRight = false;
	}

	if (key == GLUT_KEY_CTRL_L) {
		if (TYPE_BULLET == m_Object[HERO_ID]->GetPresentMode()) {
			m_Object[HERO_ID]->SetPresentMode(TYPE_PRESENT);
		}
		else {
			m_Object[HERO_ID]->SetPresentMode(TYPE_BULLET);
		}
	}
}

void ScnMgr::Update(float elapsedTime)
{
	//std::cout << std::boolalpha 
	//	<< "w: " << m_keyW << ", a: " << m_keyA << ", s: " << m_keyS << ", d: " << m_keyD << std::endl;

	//std::cout << std::boolalpha
	//	<< "Up: " << m_keyUp << ", Left: " << m_keyLeft << ", Down: " << m_keyDown << ", Right: " << m_keyRight << std::endl;

	float fx, fy, fz;
	float fAmount = 10.f;
	fx = fy = fz = 0.f;

	if (m_keyW) {
		fy += 1.f;
	}
	if (m_keyS) {
		fy -= 1.f;
	}
	if (m_keyD) {
		fx += 1.f;
	}
	if (m_keyA) {
		fx -= 1.f;
	}
	if (m_keySP) {
		fz += 1.f;
	}

	float fSize = sqrt((fx*fx) + (fy*fy));
	if (fSize > FLT_EPSILON) {
		fx /= fSize;
		fy /= fSize;

		fx *= fAmount;
		fy *= fAmount;

		m_Object[HERO_ID]->AddForce(fx, fy, 0, elapsedTime);
	}
	if (fz > FLT_EPSILON) {
		POSITION p;
		p = m_Object[HERO_ID]->GetPos();
		if (p.zPos < FLT_EPSILON) {
			fz *= fAmount * 20.f;
			m_Object[HERO_ID]->AddForce(0, 0, fz, elapsedTime);
		}
	}

	//////////////////////////////////////////////////////////////
	for (int i = 1; i < MAX_ALIVE_OBJECT; ++i) {
		if (NULL == m_Object[i]) continue;
		if (TYPE_ENEMY != m_Object[i]->GetType()) continue;

		m_Object[i]->FindHERO(m_Object[HERO_ID]);

		float fx_enemy, fy_enemy, fz_enemy;
		float fAmount_enemy = 10.f;
		fx_enemy = fy_enemy = fz_enemy = 0.f;

		if (PEACE == m_Object[i]->GetState()) {
			int dir = m_Object[i]->GetMoveDir();
			float age = m_Object[i]->GetAge();
			if (0 == ((int)age % 5)) {
				dir = rand() % 5;
				m_Object[i]->SetMoveDir(dir);
			}
			//cout << dir << endl;
			switch (dir) {
			case 0:
				m_Object[i]->SetDirection(RIGHT);
				fx_enemy += 1.f;
				break;
			case 1:
				m_Object[i]->SetDirection(LEFT);
				fx_enemy -= 1.f;
				break;
			case 2:
				if (IDLE == m_Object[i]->GetDirection()) {
					m_Object[i]->SetDirection(RIGHT);
				}
				fy_enemy += 1.f;
				break;
			case 3:
				if (IDLE == m_Object[i]->GetDirection()) {
					m_Object[i]->SetDirection(LEFT);
				}
				fy_enemy -= 1.f;
				break;
			case 4:
				m_Object[i]->SetDirection(IDLE);
				break;
			}
		}
		else if (CHASING == m_Object[i]->GetState()) {
			float x_dir = m_Object[HERO_ID]->GetPos().xPos - m_Object[i]->GetPos().xPos;
			float y_dir = m_Object[HERO_ID]->GetPos().yPos - m_Object[i]->GetPos().yPos;

			if (x_dir < FLT_EPSILON) {
				m_Object[i]->SetDirection(LEFT);
				fx_enemy -= 1.f;
			}
			if (x_dir > FLT_EPSILON) {
				m_Object[i]->SetDirection(RIGHT);
				fx_enemy += 1.f;
			}
			if (y_dir < FLT_EPSILON) {
				if (IDLE == m_Object[i]->GetDirection()) {
					m_Object[i]->SetDirection(LEFT);
				}
				fy_enemy -= 1.f;
			}
			if (y_dir > FLT_EPSILON) {
				if (IDLE == m_Object[i]->GetDirection()) {
					m_Object[i]->SetDirection(RIGHT);
				}
				fy_enemy += 1.f;
			}
		}
		float fSize_enemy = sqrt((fx_enemy*fx_enemy) + (fy_enemy*fy_enemy));
		if (fSize_enemy > FLT_EPSILON) {
			fx_enemy /= fSize_enemy;
			fy_enemy /= fSize_enemy;

			fx_enemy *= fAmount_enemy;
			fy_enemy *= fAmount_enemy;

			m_Object[i]->AddForce(fx_enemy, fy_enemy, 0, elapsedTime);
		}
	}

	/////////////////////////////////////////////////////////////
	float fxBullet, fyBullet, fzBullet;
	float vAmountBullet = 10.f;
	fxBullet = fyBullet = fzBullet = 0.f;

	if (m_keyUp) {
		fyBullet += 1.f;
	}
	if (m_keyDown) {
		fyBullet -= 1.f;
	}
	if (m_keyRight) {
		fxBullet += 1.f;
	}
	if (m_keyLeft) {
		fxBullet -= 1.f;
	}

	float fbSize = sqrtf((fxBullet*fxBullet) + (fyBullet*fyBullet) + (fzBullet*fzBullet));

	//
	if (fbSize > FLT_EPSILON) {
		fxBullet /= fbSize;
		fyBullet /= fbSize;
		fzBullet /= fbSize;

		fxBullet *= vAmountBullet;
		fyBullet *= vAmountBullet;
		fzBullet *= vAmountBullet;

		// Add object for firing bullet
		Velocity HeroVel;
		HeroVel = m_Object[HERO_ID]->GetVel();
		HeroVel.xVel = HeroVel.xVel + fxBullet;
		HeroVel.yVel = HeroVel.yVel + fyBullet;
		HeroVel.zVel = 0.f;

		Position pos;
		pos = m_Object[HERO_ID]->GetPos();
		float size = 0.4f;
		float mass = 0.3f;
		float fricCoef = 0.5f;
		Color c = { 1.f ,1.f, 1.f, 1.f };
		int type = m_Object[HERO_ID]->GetPresentMode();
		
		// Check whether this obj can shoot bullet or not
		if (TYPE_BULLET == type) {
			if (m_Object[HERO_ID]->CanShootBullet()) {
				int temp = AddObject(
					pos.xPos, pos.yPos + 0.2f, pos.zPos,
					size, size, size,
					1.f, 1.f, 1.f, 1.f,
					HeroVel.xVel, HeroVel.yVel, HeroVel.zVel,
					mass,
					fricCoef,
					10.0f,
					type);
				m_Object[temp]->SetParentObj(m_Object[HERO_ID]);
				m_Object[temp]->SetTextureID(m_snowballTexture);
				m_Object[HERO_ID]->ResetShootBulletCoolTime();
				// sound
				m_Sound->PlayShortSound(m_shootSnowballSound, false, 1.f);
			}
		}
		else if (TYPE_PRESENT == type) {
			if (m_Object[HERO_ID]->CanShootPresent()) {
				int temp = AddObject(
					pos.xPos, pos.yPos + 0.2f, pos.zPos,
					size + 0.1f, size + 0.1f, size,
					1.f, 1.f, 1.f, 1.f,
					HeroVel.xVel, HeroVel.yVel, HeroVel.zVel,
					mass,
					fricCoef,
					2.0f,
					type);
				m_Object[temp]->SetParentObj(m_Object[HERO_ID]);
				m_Object[temp]->SetTextureID(m_presentTexture);
				m_Object[HERO_ID]->ResetShootPresentCoolTime();
				--m_maxPresentCount;
				// sound
				m_Sound->PlayShortSound(m_shootSnowballSound, false, 1.f);
			}
		}
	}

	for (int src = 0; src < MAX_ALIVE_OBJECT; ++src)
	{
		for (int trg = src + 1; trg < MAX_ALIVE_OBJECT; ++trg)
		{
			if (NULL != m_Object[src] && NULL != m_Object[trg]) 
			{
				//Overlap Test
				if (m_Physics->IsOverlap(m_Object[src], m_Object[trg])) 
				{
					// Collision processing
					//std::cout << "Collision " << src << ", " << trg << std::endl;
					if (!m_Object[src]->IsAncestor(m_Object[trg]) &&
						!m_Object[trg]->IsAncestor(m_Object[src]))
					{
						if (m_Object[src]->GetType() != m_Object[trg]->GetType())
						{
							if (true == m_Object[src]->GetIsDie()) continue;
							if (true == m_Object[trg]->GetIsDie()) continue;

							m_Physics->ProcessCollision(m_Object[src], m_Object[trg]);
							m_Sound->PlayShortSound(m_hitSound, false, 1.f);
							float srcHP, trgHP;
							srcHP = m_Object[src]->GetHP();
							trgHP = m_Object[trg]->GetHP();
							if (HERO_ID == src) {
								if (true == m_Object[src]->GetIsDamage()) continue;
								m_Object[src]->Damage(trgHP);
								m_Object[src]->SetIsDamage(true);
								float time = m_Object[src]->GetAge();
								m_Object[src]->SetDamageTime(time);
								continue;
							}
							if (TYPE_PRESENT == m_Object[src]->GetType()) {
								m_Object[HERO_ID]->Damage(10.f);
								m_Object[src]->Damage(trgHP);
								continue;
							}
							else if (TYPE_PRESENT == m_Object[trg]->GetType()) {
								m_Object[HERO_ID]->Damage(10.f);
								m_Object[trg]->Damage(srcHP);
							}
							else {
								m_Object[trg]->Damage(srcHP);
								m_Object[src]->Damage(trgHP);
								continue;
							}
						}
					}
				}
			}
		}
	}

	for (auto& obj : m_Object) {
		if (NULL == obj) continue;
		if (TYPE_PRESENT != obj->GetType()) continue;
		for (auto& nor : m_Normal) {
			if (NULL == nor) continue;
			if (m_Physics->IsOverlap(obj, nor)) {
				if (TYPE_HOUSE == nor->GetType()) {
					if (true == nor->GetIsCollision()) continue;
					nor->SetIsCollision(true);
					nor->SetColor(1.f, 1.f, 1.f, 1.f);
					++m_presentCount;
					m_Sound->PlayShortSound(m_goalSound, false, 1.f);
					
				}
				obj->Damage(obj->GetHP());
			}
		}
	}

	for (int i = 0; i < MAX_ALIVE_OBJECT; ++i) {
		if (NULL != m_Object[i])
			m_Object[i]->Update(elapsedTime);
	}

	// 카메라 캐릭터 따라서 이동
	POSITION p;
	p = m_Object[HERO_ID]->GetPos();
	m_Renderer->SetCameraPos(p.xPos * 100.f, p.yPos * 100.f);	// pixel
}

int ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);
	
	// 집 UI
	POSITION h_p = m_Object[HERO_ID]->GetPos();
	m_Renderer->DrawTextureRect(
		h_p.xPos * 100.f - 1000.f, h_p.yPos* 100.f + 850.f, h_p.zPos,
		100.f, 100.f, 1.f,
		1.f, 1.f, 1.f, 1.f,
		m_houseTexture[1], false
	);

	m_Renderer->DrawTextureRectAnim(
		h_p.xPos * 100.f - 900.f, h_p.yPos * 100.f + 800.f, h_p.zPos,
		200.f, 200.f, 1.f,
		1.f, 1.f, 1.f, 1.f,
		m_numForUITexture,
		1, 20,
		0, OBSTACLE_START_NUM - m_presentCount - 1,
		false
	);

	// 선물 UI
	m_Renderer->DrawTextureRect(
		h_p.xPos * 100.f - 800.f, h_p.yPos* 100.f + 850.f, h_p.zPos,
		100.f, 100.f, 1.f,
		1.f, 1.f, 1.f, 1.f,
		m_presentTexture, false
	);

	m_Renderer->DrawTextureRectAnim(
		h_p.xPos * 100.f - 700.f, h_p.yPos * 100.f + 800.f, h_p.zPos,
		200.f, 200.f, 1.f,
		1.f, 1.f, 1.f, 1.f,
		m_numForUITexture,
		1, 20,
		0, m_maxPresentCount - 1,
		false
	);

	// background
	m_Renderer->DrawGround(0.f, 0.f, 0.f, 8000.f, 8000.f, 1.f, 1.f, 1.f, 1.f, 1.f, m_sideTexture, 0.7f);
	m_Renderer->DrawGround(0.f, 0.f, 0.f, 10000.f, 10000.f, 1.f, 0.93f, 0.93f, 0.93f, 1.f, m_backgroundTexture, 1.f);

	// particle
	// DrawParticle -> 실제위치 (어디서 시작), Dir은 가속도
	static float pTime = 0.f;
	pTime += 0.016;
	m_Renderer->DrawParticle(
		m_snow,
		0.f, 4000.f, 0.f,
		1.f,
		1.f, 1.f, 1.f, 1.f,
		0.f, -70.f,
		m_snowTexture,
		1.f,
		pTime, 0.9f
	);
	m_Renderer->DrawParticle(
		m_snow,
		0.f, -70.f, 0.f,
		1.f,
		1.f, 1.f, 1.f, 1.f,
		0.f, -2000.f,
		m_snowTexture,
		1.f,
		pTime, 0.9f
	);

	// Renderer Test
	Position p {};
	Volume v {};
	Color c {};
	int texID = -1;

	for (int i = 0; i < MAX_ALIVE_OBJECT; ++i) {
		if (NULL != m_Object[i]) {
			p = m_Object[i]->GetPos();
			p.xPos = p.xPos * 100.f;
			p.yPos = p.yPos * 100.f;
			p.zPos = p.zPos * 100.f;

			v = m_Object[i]->GetVol();
			v.xVol = v.xVol * 100.f;
			v.yVol = v.yVol * 100.f;
			v.zVol = v.zVol * 100.f;
			c = m_Object[i]->GetColor();
			texID = m_Object[i]->GetTextureID();

			//m_Renderer->DrawSolidRect(p.xPos, p.yPos, p.zPos, v.xVol, c.red, c.green, c.blue, c.alpha);
			if (i == HERO_ID) {
				if (m_Object[i]->GetDirection() == LEFT) {
					float textureNum = m_Object[i]->GetAge() * 13.f;
					textureNum = (int)textureNum % 8;
					m_Renderer->DrawTextureRectAnim(
						p.xPos, p.yPos, p.zPos,
						v.xVol, v.yVol, v.zVol,
						c.red, c.green, c.blue, c.alpha,
						texID,
						8, 1,
						7 - (int)textureNum, 0
					);
				}
				else if (m_Object[i]->GetDirection() == RIGHT) {
					float textureNum = m_Object[i]->GetAge() * 13.f;
					textureNum = (int)textureNum % 8;
					m_Renderer->DrawTextureRectAnim(
						p.xPos, p.yPos, p.zPos,
						v.xVol, v.yVol, v.zVol,
						c.red, c.green, c.blue, c.alpha,
						texID,
						8, 1,
						(int)textureNum, 0
					);
				}
				else {
					float textureNum = m_Object[i]->GetAge() * 5.f;
					textureNum = (int)textureNum % 5;
					m_Renderer->DrawTextureRectAnim(
						p.xPos, p.yPos, p.zPos,
						v.xVol, v.yVol, v.zVol,
						c.red, c.green, c.blue, c.alpha,
						texID,
						8, 1,
						(int)textureNum, 0
					);
				}
				DrawGauge(i, p, v);
			}
			else {
				if (m_Object[i]->GetType() == TYPE_BULLET || 
					m_Object[i]->GetType() == TYPE_PRESENT) {
					m_Renderer->DrawTextureRect(
						p.xPos, p.yPos, p.zPos,
						v.xVol, v.yVol, v.zVol,
						c.red, c.green, c.blue, c.alpha,
						texID, false
					);

					if (m_Object[i]->GetType() == TYPE_PRESENT) {
						VELOCITY vel = m_Object[i]->GetVel();
						m_Renderer->DrawParticle(
							m_star,
							p.xPos, p.yPos, p.zPos,
							1.f,
							1.f, 1.f, 1.f, 1.f,
							-vel.xVel * 10.f, -vel.yVel * 10.f,
							m_starTexture,
							1.f,
							m_Object[i]->GetAge() * 5, 0.7f
						);
					}
				}
				else {
					if (true == m_Object[i]->GetIsDie()) continue;
					if (m_Object[i]->GetDirection() == LEFT) {
						float textureNum = m_Object[i]->GetAge() * 13.f;
						textureNum = (int)textureNum % 6;
						m_Renderer->DrawTextureRectAnim(
							p.xPos, p.yPos, p.zPos,
							v.xVol, v.yVol, v.zVol,
							c.red, c.green, c.blue, c.alpha,
							texID,
							6, 1,
							5 - (int)textureNum, 0
						);
					}
					else if (m_Object[i]->GetDirection() == RIGHT) {
						float textureNum = m_Object[i]->GetAge() * 13.f;
						textureNum = (int)textureNum % 6;
						m_Renderer->DrawTextureRectAnim(
							p.xPos, p.yPos, p.zPos,
							v.xVol, v.yVol, v.zVol,
							c.red, c.green, c.blue, c.alpha,
							texID,
							6, 1,
							(int)textureNum, 1
						);
					}
					else {
						float textureNum = m_Object[i]->GetAge() * 3.f;
						textureNum = (int)textureNum % 4;
						m_Renderer->DrawTextureRectAnim(
							p.xPos, p.yPos, p.zPos,
							v.xVol, v.yVol, v.zVol,
							c.red, c.green, c.blue, c.alpha,
							texID,
							4, 1,
							(int)textureNum, 0
						);
					}
					DrawGauge(i, p, v);
				}
			}
		}
	}

	for (int i = 0; i < MAX_NORMAL_OBJECT; ++i) {
		if (NULL == m_Normal[i]) continue;
		p = m_Normal[i]->GetPos();
		v = m_Normal[i]->GetVol();
		c = m_Normal[i]->GetCol();
		p.xPos = p.xPos * 100.f;
		p.yPos = p.yPos * 100.f;
		p.zPos = p.zPos * 100.f - 50.f;
		v.xVol = v.xVol * 100.f;
		v.yVol = v.yVol * 100.f;
		v.zVol = v.zVol * 100.f;
		texID = m_Normal[i]->GetTextureID();
		m_Renderer->DrawTextureRect(
			p.xPos, p.yPos, p.zPos,
			v.xVol, v.yVol, v.zVol,
			c.red, c.green, c.blue, c.alpha,
			texID, true
		);
	}
	DoGarbageCollection();

	return m_state;
}

void ScnMgr::DrawGauge(int index, POSITION p, VOLUME v)
{
	m_Renderer->DrawSolidRectGauge(
		p.xPos, p.yPos, p.zPos,
		0.f, v.yVol / 2.f + 5.f, 0.f,
		v.xVol, 10.f, 1.f,
		1.f, 0.f, 0.f, 1.f,
		m_Object[index]->GetHPGauge() * 100.f);
}

void ScnMgr::DoGarbageCollection()
{
	// 게임 끝 조건
	if (m_presentCount == OBSTACLE_START_NUM) {
		m_state = END;
		return;
	}
	if (m_Object[HERO_ID]->GetHP() < FLT_EPSILON || 
		m_maxPresentCount == 0) 
	{
		m_state = FAIL;
		return;
	}

	// Delete bullets those zero vel
	for (int i = 1; i < MAX_ALIVE_OBJECT; ++i) {
		if (m_Object[i] != NULL) {
			if (m_Object[i]->GetType() == TYPE_BULLET || 
				m_Object[i]->GetType() == TYPE_PRESENT) 
			{
				Velocity v;
				v = m_Object[i]->GetVel();
				float vSize = sqrtf(v.xVel*v.xVel + v.yVel*v.yVel + v.zVel*v.zVel);
				if (vSize < FLT_EPSILON) {
					DeleteObject(i);
					continue;
				}
			}
			float hp;
			hp = m_Object[i]->GetHP();
			if (hp < FLT_EPSILON) {
				if (m_Object[i]->GetType() == TYPE_BULLET ||
					m_Object[i]->GetType() == TYPE_PRESENT)
				{
					DeleteObject(i);
				}
				else {
					if (true == m_Object[i]->GetIsDie()) continue;
					float now = m_Object[i]->GetAge();
					m_Object[i]->SetDieTime(now);
					m_Object[i]->SetIsDie(true);
					++m_maxPresentCount;
				}
				continue;
			}
		}
	}

}